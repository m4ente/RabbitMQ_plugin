This are some scripts to monitor RabbitMQ (www.rabbitmq.com) using the
formerly Nagios-based monitoring software Check_MK 
(www.mathias-kettner.de/check_mk.html, www.omdistro.org).

20170410: Initial commit. Functionality:
reporting which queues are available, comparing their incoming messages
to their outgoing messages rate and considering a threshold for the 
total number of messages in the queue.
